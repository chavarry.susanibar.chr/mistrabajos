package com.yo.practica01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnVerificar.setOnClickListener {
            val nacimiento =edtNacimiento.text.toString().toInt()
            imgMoticons.isVisible=true

            if (nacimiento in 1930 .. 1948){
                tvResultado.text = "6,300.000"
                imgMoticons.setImageResource(R.drawable.icon_sonrisa)
            }else if (nacimiento in 1949 .. 1968){
                tvResultado.text = "12,200.000"
                imgMoticons.setImageResource(R.drawable.icon_mareado)
            }else if (nacimiento in 1969 .. 1980 ){
                tvResultado.text = "9,300.000"
                imgMoticons.setImageResource(R.drawable.icon_lengua)
            }else if (nacimiento in 1981 .. 1993){
                tvResultado.text = "7,200.000"
                imgMoticons.setImageResource(R.drawable.icon_enojado)
            }else if (nacimiento in 1994 .. 2010){
                tvResultado.text = "7,800.000"

                imgMoticons.setImageResource(R.drawable.icon_alterado)
            }else {
                tvResultado.text = "Usted no pertenece a ninguna generación"
            }
        }


    }
}