package com.yo.practica2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.activity_destino.*

class DestinoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_destino)

        val bundleRecepcion = intent.extras

        val nombres = bundleRecepcion!!.getString("key_nombre")
        val edad = bundleRecepcion!!.getString("key_edad")
        val tipo = bundleRecepcion!!.getString("key_tipo")
        val vacunas = bundleRecepcion!!.getString("key_vacunas")



        edtNombrePerro.text= "Nombre: $nombres"
        twEdad.text="Edad: $edad"
        twTipo.text="Tipo de mascota: $tipo"
        twVacuna.text ="Vacunas: $vacunas"

        if (tipo == "Perro"){
            imgPerro.isVisible=true
        }else if (tipo =="Gato"){
            imgGato.isVisible=true
        }else {
            imgConejo.isVisible=true
        }





    }
}