package com.yo.practica2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_destino.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {




    override fun onCreate(savedInstanceState: Bundle?) {


        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        ///////////////////////////////////////////////////////

        edtNombre.setText("")
        edtEdad.setText("")
        
        btnRegistro.setOnClickListener {

            val nombre= edtNombre.text.toString()
            val edad = edtEdad.text.toString()
            var mascota = ""
            var vacunas =""



            if (nombre.isEmpty()){
                Toast.makeText(this,"Debe ingresar el nombre",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (edad.isEmpty()){
                Toast.makeText(this,"Debe ingresar la edad",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (rbPerro.isChecked){
                mascota = "Perro"


            }else if (rbGato.isChecked){
                mascota = "Gato"


            }else{
                mascota = "Conejo"

            }

            if (chbMoquillo.isChecked){
                vacunas = "Moquillo"
            }else if (chbHepatitis.isChecked){
                vacunas = "Hepatitis infecciosa"
            }else if (chbParvovirus.isChecked){
                vacunas = "Parvovirus"
            }else if (chbRabia.isChecked){
                vacunas = "Rabia"
            }else if (chbNinguna.isChecked){
                vacunas = "Ninguna"
            }else{
                Toast.makeText(this,"Debe indicar vacunas",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            val bundle = Bundle()
            bundle.putString("key_nombre",nombre)
            bundle.putString("key_edad",edad)
            bundle.putString("key_tipo",mascota)
            bundle.putString("key_vacunas",vacunas)

            



            val cambioPantalla2 = Intent(this, DestinoActivity::class.java)
            cambioPantalla2.putExtras(bundle)
            startActivity(cambioPantalla2)




        }
    }
}